<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Usuario;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Transactions');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Crear transacción'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'points',
            'type',
            [
                'attribute' => 'creator_ID',
                'value' => function($data)
                {
                    $user = Usuario::findOne($data->creator_ID);
                    return $user->username; 
                }
            ],
            'creation_date',
            'observation',
            'product_ID',
            [
                'attribute' => 'status',
                'value' => function($data)
                {
                    return ($data->status == 1) ? 'Activo' : 'Inactivo';
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
