<?php

use app\models\Product;
use yii\helpers\Html;
use app\models\Usuario;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Transaction */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Transacción'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="transaction-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Estás seguro que quieres eliminar este item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'points',
            'type',
            
            [
                'attribute' => 'creator_ID',
                'value' => function($data)
                {
                    $user = Usuario::findOne($data->creator_ID);
                    return $user->username; 
                }
            ],
            'creation_date',
            'observation',
            [
                'attribute' => 'product_ID',
                'value' => function($data)
                {
                    $product = Product::findOne($data->product_ID);
                    return $product->name;
                }
            ],
            [
                'attribute' => 'status',
                'value' => function($data)
                {
                    return ($data->status == 1) ? 'Activo' : 'Inactivo';
                }
            ],
        ],
    ]) ?>

</div>
