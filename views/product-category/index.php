<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Usuario;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Categoria producto');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Crear categoria producto'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'name_product',
            [
                'attribute' => 'creator_ID',
                'value' => function($data)
                {
                    $user = Usuario::findOne($data->creator_ID);
                    return $user->username; 
                }
            ],
            'creation_date',
            [
                'attribute' => 'status',
                'value' => function($data)
                {
                    return ($data->status == 1) ? 'Activo' : 'Inactivo';
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
