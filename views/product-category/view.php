<?php

use app\models\User;
use app\models\Usuario;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProductCategory */

$this->title = $model->name_product;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categoria productos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Estás seguro que quieres eliminar este item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'name_product',
            [
                'attribute' => 'creator_ID',
                'value' => function($data)
                {
                    $user = Usuario::findOne($data->creator_ID);
                    return $user->username; 
                }
            ],
            'creation_date',
            [
                'attribute' => 'status',
                'value' => function($data)
                {
                    return ($data->status == 1) ? 'Activo' : 'Inactivo';
                }
            ]
        ],
    ]) ?>

</div>
