<?php

use yii\helpers\Html;
use app\models\Usuario;

    $id = $_GET['id'];
    $model = Usuario::findOne($id);

?>

    <h1><?= $model->username ?></h1>
    <br>


    <?php echo Html::beginForm(); ?>
 
    <?php echo Html::errorSummary($model); ?>
 
    <div class="row">
        <?php echo Html::activeLabel($model,'puntos'); ?>
        <?php echo Html::activeTextInput($model,'puntos', ['class' => 'form-control']); ?>
        <br>
    </div>

    <div class="row submit">
        <?php echo Html::submitButton('Enviar', ['class' => 'btn btn-primary'], function ($model){
            $model->save();
        }); ?>
    </div>
 
    <?php echo Html::endForm();?>

