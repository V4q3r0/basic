<?php

use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Puntos usuarios');
?>
<div class="site-puntos">

    <h1><?= Html::encode($this->title) ?></h1>

    <style>
        .espacio{
            padding: 5px;
        }
    </style>

    <table class="table table-hover table-bordered" style="width: 50%; margin: auto;">
         <tr>
            <th class="espacio">Nombre</th>
            <th class="espacio">Puntos</th>
            <th class="espacio">Asignar puntos</th>
        </tr>
        <?php foreach ($usuarios as $usuario): ?>
            
            <tr>
                <td class="espacio"><?= $usuario->username ?></td>
                <td class="espacio"><?= $usuario->puntos ?></td>
                <td class="espacio"><a class="btn btn-info" href="<?= Url::to(['site/asignar', 'id' => $usuario->id]) ?>">Asginar puntos</a></td>
            </tr>
           
        <?php endforeach; ?>
    </table>


</div>