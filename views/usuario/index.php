<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Usuarios');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Crear usuario'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            [
                'attribute' => 'rol',
                'value' => function($data)
                {
                    return ($data->rol == 1) ? 'Adminsitrador' : 'Operativo';
                }
            ],
            [
                'attribute' => 'estado',
                'value' => function($data)
                {
                    return ($data->estado == 1) ? 'Activo' : 'Inactivo';
                }
            ],
            'puntos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
