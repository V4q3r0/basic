<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_category".
 *
 * @property int $ID
 * @property string $name_product
 * @property int $creator_ID
 * @property string $creation_date
 * @property int $status
 *
 * @property Usuario $creator
 */
class ProductCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_product', 'creator_ID', 'status'], 'required'],
            [['creator_ID', 'status'], 'integer'],
            [['creation_date'], 'safe'],
            [['name_product'], 'string', 'max' => 120],
            [['creator_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['creator_ID' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'name_product' => Yii::t('app', 'Nombre categoria'),
            'creator_ID' => Yii::t('app', 'Creador'),
            'creation_date' => Yii::t('app', 'Fecha de creación'),
            'status' => Yii::t('app', 'Estado'),
        ];
    }

    /**
     * Gets query for [[Creator]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'creator_ID']);
    }
}
