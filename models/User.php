<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $auth_Key;
    public $access_Token;
    public $rol;
    public $estado;
    public $puntos;

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        $user = Usuario::find()->where(['estado' => 1, 'id' => $id])->limit(1)->one();
        return isset($user) ? new static($user) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user = Usuario::find()->where(['estado' => 1, 'access_Token' => $token])->limit(1)->one();
        return isset($user) ? new static($user) : null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $user = Usuario::find()->where(['estado' => 1, 'username' => $username])->limit(1)->one();
        return isset($user) ? new static($user) : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_Key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($auth_Key)
    {
        return $this->auth_Key === $auth_Key;
    }

    public function getPuntos()
    {
        return $this->puntos;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === crypt($password, Yii::$app->params['pHash']);
    }

    public static function getUsername($id)
    {
        $user = Usuario::find()->where(['estado' => 1, 'id' => $id])->limit(1)->one();
        return isset($user) ? new static($user) : null;
    }

}
