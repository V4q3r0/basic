<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaction".
 *
 * @property int $ID
 * @property int $points
 * @property int $type
 * @property string $observation
 * @property int $creator_ID
 * @property string $creation_date
 * @property int $product_ID
 * @property int $status
 *
 * @property Usuario $creator
 * @property Product $product
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['points', 'type', 'observation', 'creator_ID', 'product_ID', 'status'], 'required'],
            [['points', 'type', 'creator_ID', 'product_ID', 'status'], 'integer'],
            [['creation_date'], 'safe'],
            [['observation'], 'string', 'max' => 240],
            [['creator_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['creator_ID' => 'id']],
            [['product_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_ID' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'points' => Yii::t('app', 'Puntos'),
            'type' => Yii::t('app', 'Tipo'),
            'observation' => Yii::t('app', 'Observación'),
            'creator_ID' => Yii::t('app', 'Creado por'),
            'creation_date' => Yii::t('app', 'Fecha de creación'),
            'product_ID' => Yii::t('app', 'Producto'),
            'status' => Yii::t('app', 'Estado'),
        ];
    }

    /**
     * Gets query for [[Creator]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'creator_ID']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['ID' => 'product_ID']);
    }
}
