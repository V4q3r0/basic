<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $ID
 * @property string $name
 * @property int $creator_ID
 * @property string $creation_date
 * @property int $category
 * @property int $point_claim
 * @property string $instructions
 * @property int $photo
 * @property string $details
 *
 * @property Transaction[] $transactions
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'creator_ID', 'category', 'point_claim', 'instructions', 'photo', 'details'], 'required'],
            [['creator_ID', 'category', 'point_claim', 'photo'], 'integer'],
            [['creation_date'], 'safe'],
            [['name'], 'string', 'max' => 120],
            [['instructions', 'details'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'creator_ID' => Yii::t('app', 'Creator ID'),
            'creation_date' => Yii::t('app', 'Creation Date'),
            'category' => Yii::t('app', 'Category'),
            'point_claim' => Yii::t('app', 'Point Claim'),
            'instructions' => Yii::t('app', 'Instructions'),
            'photo' => Yii::t('app', 'Photo'),
            'details' => Yii::t('app', 'Details'),
        ];
    }

    /**
     * Gets query for [[Transactions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['product_ID' => 'ID']);
    }
}
