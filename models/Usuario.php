<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string|null $auth_Key
 * @property string|null $access_Token
 * @property int $rol
 * @property int $estado
 * @property int $puntos
 *
 * @property ProductCategory[] $productCategories
 * @property Transaction[] $transactions
 */
class Usuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password', 'puntos'], 'required'],
            [['rol', 'estado', 'puntos'], 'integer'],
            [['username'], 'string', 'max' => 30],
            [['password', 'auth_Key', 'access_Token'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Nombre'),
            'password' => Yii::t('app', 'Password'),
            'auth_Key' => Yii::t('app', 'Auth Key'),
            'access_Token' => Yii::t('app', 'Access Token'),
            'rol' => Yii::t('app', 'Rol'),
            'estado' => Yii::t('app', 'Estado'),
            'puntos' => Yii::t('app', 'Puntos'),
        ];
    }

    /**
     * Gets query for [[ProductCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategories()
    {
        return $this->hasMany(ProductCategory::className(), ['creator_ID' => 'id']);
    }

    /**
     * Gets query for [[Transactions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTransactions()
    {
        return $this->hasMany(Transaction::className(), ['creator_ID' => 'id']);
    }

}
